-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 08-02-2020 a las 10:09:58
-- Versión del servidor: 10.3.15-MariaDB
-- Versión de PHP: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `venta_de_muebles`
--
CREATE DATABASE IF NOT EXISTS `venta_de_muebles` DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish_ci;
USE `venta_de_muebles`;

DELIMITER $$
--
-- Procedimientos
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `getCategorias` ()  BEGIN
SELECT * FROM categoria;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `getMarcas` ()  BEGIN
SELECT * FROM marca;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `getProveedores` ()  BEGIN
SELECT * FROM proveedores;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `pa_consultar` ()  BEGIN
SELECT p.id_producto, p.nombre_producto, p.precio,p.stock,pro.proveedor,m.marca,cat.nombres
from producto p 
INNER JOIN proveedores pro on pro.id_proveedor = p.id_proveedor
INNER JOIN marca m on m.id_marca = p.id_marca
INNER JOIN categoria cat on cat.id_categoria = p.id_categoria;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `pa_consultarporid` (IN `pa_id_producto` INT)  BEGIN
SELECT * FROM producto
WHERE id_producto = pa_id_producto;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `pa_eliminar` (IN `pa_id_producto` INT)  BEGIN
DELETE FROM producto
WHERE id_producto = pa_id_producto;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `pa_insertar` (IN `pa_nombre_producto` VARCHAR(30), IN `pa_precio` DECIMAL, IN `pa_stock` INT, IN `pa_id_proveedor` INT, IN `pa_id_marca` INT, IN `pa_id_categoria` INT)  NO SQL
BEGIN
INSERT INTO producto(nombre_producto,precio,stock,id_proveedor,id_marca,id_categoria)
VALUES(pa_nombre_producto,pa_precio,pa_stock,pa_id_proveedor,pa_id_marca,pa_id_categoria);
END$$

CREATE DEFINER=`root`@`127.0.0.1` PROCEDURE `sp_consultarClientes` ()  BEGIN
SELECT c.id_cliente,c.nombre_cliente,c.apellido_cliente,c.dui_cliente,c.direccion,c.fecha_nacimiento,c.telefono_cliente,c.email
FROM cliente c;
END$$

CREATE DEFINER=`root`@`127.0.0.1` PROCEDURE `sp_consultarFacturas` ()  BEGIN 
SELECT f.num_factura,c.nombre_cliente,c.apellido_cliente,c.dui_cliente,f.fecha,m.nombres
FROM factura f
INNER JOIN cliente c INNER JOIN modo_pago m ON
c.id_cliente=f.id_cliente AND m.num_pago=f.num_pago;
END$$

CREATE DEFINER=`root`@`127.0.0.1` PROCEDURE `sp_consultarIdFactura` (IN `pa_num_factura` INT)  BEGIN
SELECT*FROM factura WHERE num_factura = pa_num_factura;
END$$

CREATE DEFINER=`root`@`127.0.0.1` PROCEDURE `sp_consultarPago` ()  BEGIN
SELECT*FROM modo_pago;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_editar` (IN `pa_id_producto` INT, IN `pa_nombre_producto` VARCHAR(30), IN `pa_precio` DECIMAL, IN `pa_stock` INT, IN `pa_id_proveedor` INT, IN `pa_id_marca` INT, IN `pa_id_categoria` INT)  BEGIN
UPDATE producto SET
nombre_producto=pa_nombre_producto,
precio=pa_precio,
stock=pa_stock,
id_proveedor=pa_id_proveedor,
id_marca=pa_id_marca,
id_categoria=pa_id_categoria
WHERE id_producto=pa_id_producto;
END$$

CREATE DEFINER=`root`@`127.0.0.1` PROCEDURE `sp_editarFactura` (IN `pa_num_factura` INT, IN `pa_id_cliente` INT, IN `pa_fecha` DATE, IN `pa_num_pago` INT)  BEGIN
UPDATE factura SET
id_cliente=pa_id_cliente,
fecha=pa_fecha,
num_pago=pa_num_pago
WHERE num_factura=pa_num_factura;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_eliminarCliente` (IN ` pa_id_cliente` INT)  BEGIN
DELETE FROM cliente WHERE id_cliente = pa_id_cliente;
END$$

CREATE DEFINER=`root`@`127.0.0.1` PROCEDURE `sp_eliminarFcatura` (IN `pa_num_factura` INT)  BEGIN
DELETE FROM factura WHERE num_factura = pa_num_factura;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_insertarCliente` (IN `pa_nombre_cliente` VARCHAR(30), IN `pa_apellido_cliente` VARCHAR(30), IN `pa_dui_cliente` VARCHAR(10), IN `pa_direccion` VARCHAR(75), IN `pa_fecha_nacimiento` DATE, IN `pa_telefono_cliente` VARCHAR(10), IN `pa_email` VARCHAR(75))  BEGIN
INSERT INTO cliente(nombre_cliente,apellido_cliente,dui_cliente,direccion,fecha_nacimiento,telefono_cliente,email)
VALUES (pa_nombre_cliente,pa_apellido_cliente,pa_dui_cliente,pa_direccion,pa_fecha_nacimiento,pa_telefono_cliente,pa_email);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_insertarDetalle` (IN `pa_id_factura` INT, IN `pa_id_producto` INT, IN `pa_cantidad` INT, IN `pa_precio` DOUBLE)  BEGIN
INSERT INTO detalle(id_factura,id_producto,cantidad,precio)
VALUES (pa_id_factura,pa_id_producto,pa_cantidad,pa_precio);
END$$

CREATE DEFINER=`root`@`127.0.0.1` PROCEDURE `sp_insertarFactura` (IN `pa_num_factura` INT, IN `pa_id_cliente` INT, IN `pa_fecha` DATE, IN `pa_num_pago` INT)  BEGIN
INSERT INTO factura(num_factura,id_cliente,fecha,num_pago)
VALUES (pa_num_factura,pa_id_cliente,pa_fecha,pa_num_pago);
END$$

CREATE DEFINER=`root`@`127.0.0.1` PROCEDURE `sp_login` (IN `usu` VARCHAR(50), IN `cla` VARCHAR(50))  BEGIN
SELECT * from usuario WHERE usuario = usu and clave = cla;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria`
--

CREATE TABLE `categoria` (
  `id_categoria` int(11) NOT NULL,
  `nombres` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `descripcion` text COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `categoria`
--

INSERT INTO `categoria` (`id_categoria`, `nombres`, `descripcion`) VALUES
(1, 'Receptáculos ', 'Sirven de deposito de objetos'),
(2, 'Recostarse', 'Camas, literas, cunas, divanes.'),
(3, 'Asientos', 'Sillas, sillones, sofás, otomanes');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE `cliente` (
  `id_cliente` int(11) NOT NULL,
  `nombre_cliente` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `apellido_cliente` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `dui_cliente` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `direccion` varchar(75) COLLATE utf8_spanish_ci NOT NULL,
  `fecha_nacimiento` date NOT NULL,
  `telefono_cliente` varchar(9) COLLATE utf8_spanish_ci NOT NULL,
  `email` varchar(75) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`id_cliente`, `nombre_cliente`, `apellido_cliente`, `dui_cliente`, `direccion`, `fecha_nacimiento`, `telefono_cliente`, `email`) VALUES
(13, 'Carlos ', 'Bonilla', '7878885659', 'fuyegfhfhgsbvhgvg', '2020-02-12', '7878-7878', 'ybfdyveftgsftewtf'),
(14, 'Angela', 'Pineda', '00787870-4', 'jwhduyfy', '2020-02-04', '78787-787', 'jhbfygfyvyevy'),
(15, 'William', 'Quijano', '020298-6', 'Las Flores', '2020-02-07', '78989596', 'william@correo.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle`
--

CREATE TABLE `detalle` (
  `num_detalle` int(11) NOT NULL,
  `id_factura` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `precio` decimal(10,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `detalle`
--

INSERT INTO `detalle` (`num_detalle`, `id_factura`, `id_producto`, `cantidad`, `precio`) VALUES
(1, 990009, 15, 2, '2');

--
-- Disparadores `detalle`
--
DELIMITER $$
CREATE TRIGGER `tr_disminuye_producto` AFTER INSERT ON `detalle` FOR EACH ROW BEGIN
UPDATE producto SET 
stock= stock-new.cantidad
WHERE id_producto=id_producto;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `tr_suma_stock` AFTER DELETE ON `detalle` FOR EACH ROW BEGIN
UPDATE producto SET 
stock= stock + old.cantidad
WHERE id_producto=id_producto;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empleado`
--

CREATE TABLE `empleado` (
  `id_empleado` int(11) NOT NULL,
  `nombre` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `apellido` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `fecha_nacimiento` date NOT NULL,
  `direccion` varchar(60) COLLATE utf8_spanish_ci NOT NULL,
  `dui` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `correo` varchar(75) COLLATE utf8_spanish_ci NOT NULL,
  `telefono` varchar(9) COLLATE utf8_spanish_ci NOT NULL,
  `sexo` varchar(10) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `empleado`
--

INSERT INTO `empleado` (`id_empleado`, `nombre`, `apellido`, `fecha_nacimiento`, `direccion`, `dui`, `correo`, `telefono`, `sexo`) VALUES
(1, 'Rogelio', 'Gomez', '2020-01-21', 'rgegeg', '21212', 'sdfsfvvdv', '2122', 'Masculino');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `factura`
--

CREATE TABLE `factura` (
  `num_factura` int(11) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `num_pago` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `factura`
--

INSERT INTO `factura` (`num_factura`, `id_cliente`, `fecha`, `num_pago`) VALUES
(990009, 13, '2020-02-08', 2),
(9889988, 13, '2020-02-08', 2),
(9909990, 14, '2020-02-08', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `marca`
--

CREATE TABLE `marca` (
  `id_marca` int(11) NOT NULL,
  `marca` varchar(30) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `marca`
--

INSERT INTO `marca` (`id_marca`, `marca`) VALUES
(1, 'centrum');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `modo_pago`
--

CREATE TABLE `modo_pago` (
  `num_pago` int(11) NOT NULL,
  `nombres` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `otros_detalles` varchar(50) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `modo_pago`
--

INSERT INTO `modo_pago` (`num_pago`, `nombres`, `otros_detalles`) VALUES
(1, 'Tarjeta', 'prueba'),
(2, 'Contado', 'prueba');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto`
--

CREATE TABLE `producto` (
  `id_producto` int(11) NOT NULL,
  `nombre_producto` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `precio` decimal(10,0) NOT NULL,
  `stock` int(11) NOT NULL,
  `id_proveedor` int(11) NOT NULL,
  `id_marca` int(11) NOT NULL,
  `id_categoria` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `producto`
--

INSERT INTO `producto` (`id_producto`, `nombre_producto`, `precio`, `stock`, `id_proveedor`, `id_marca`, `id_categoria`) VALUES
(15, 'Cocina', '200', 198, 1, 1, 3),
(16, 'Mesa', '200', 198, 1, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedores`
--

CREATE TABLE `proveedores` (
  `id_proveedor` int(11) NOT NULL,
  `proveedor` varchar(30) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `proveedores`
--

INSERT INTO `proveedores` (`id_proveedor`, `proveedor`) VALUES
(1, 'sony'),
(2, 'samsumg|');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rol`
--

CREATE TABLE `rol` (
  `id_rol` int(11) NOT NULL,
  `rol` varchar(20) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `rol`
--

INSERT INTO `rol` (`id_rol`, `rol`) VALUES
(1, 'Administrador'),
(2, 'Vendedor');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `id_usuario` int(11) NOT NULL,
  `usuario` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `clave` varchar(40) COLLATE utf8_spanish_ci NOT NULL,
  `id_empleado` int(11) NOT NULL,
  `id_rol` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id_usuario`, `usuario`, `clave`, `id_empleado`, `id_rol`) VALUES
(3, 'rudy', '202cb962ac59075b964b07152d234b70', 1, 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`id_categoria`);

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`id_cliente`),
  ADD UNIQUE KEY `dui_cliente` (`dui_cliente`),
  ADD UNIQUE KEY `telefono_cliente` (`telefono_cliente`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indices de la tabla `detalle`
--
ALTER TABLE `detalle`
  ADD PRIMARY KEY (`num_detalle`,`id_factura`),
  ADD KEY `producto_detalle` (`id_producto`),
  ADD KEY `factura_detalle` (`id_factura`);

--
-- Indices de la tabla `empleado`
--
ALTER TABLE `empleado`
  ADD PRIMARY KEY (`id_empleado`),
  ADD UNIQUE KEY `dui` (`dui`),
  ADD UNIQUE KEY `correo` (`correo`),
  ADD UNIQUE KEY `telefono` (`telefono`);

--
-- Indices de la tabla `factura`
--
ALTER TABLE `factura`
  ADD PRIMARY KEY (`num_factura`),
  ADD KEY `pago_factura` (`num_pago`),
  ADD KEY `cliente_factura` (`id_cliente`);

--
-- Indices de la tabla `marca`
--
ALTER TABLE `marca`
  ADD PRIMARY KEY (`id_marca`);

--
-- Indices de la tabla `modo_pago`
--
ALTER TABLE `modo_pago`
  ADD PRIMARY KEY (`num_pago`);

--
-- Indices de la tabla `producto`
--
ALTER TABLE `producto`
  ADD PRIMARY KEY (`id_producto`),
  ADD KEY `categoria_producto` (`id_categoria`),
  ADD KEY `proveedor_producto` (`id_proveedor`),
  ADD KEY `marca_producto` (`id_marca`);

--
-- Indices de la tabla `proveedores`
--
ALTER TABLE `proveedores`
  ADD PRIMARY KEY (`id_proveedor`);

--
-- Indices de la tabla `rol`
--
ALTER TABLE `rol`
  ADD PRIMARY KEY (`id_rol`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id_usuario`),
  ADD KEY `empleado_usuarios` (`id_empleado`),
  ADD KEY `rol_usuarios` (`id_rol`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `categoria`
--
ALTER TABLE `categoria`
  MODIFY `id_categoria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `cliente`
--
ALTER TABLE `cliente`
  MODIFY `id_cliente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de la tabla `detalle`
--
ALTER TABLE `detalle`
  MODIFY `num_detalle` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `empleado`
--
ALTER TABLE `empleado`
  MODIFY `id_empleado` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `marca`
--
ALTER TABLE `marca`
  MODIFY `id_marca` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `modo_pago`
--
ALTER TABLE `modo_pago`
  MODIFY `num_pago` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `producto`
--
ALTER TABLE `producto`
  MODIFY `id_producto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT de la tabla `proveedores`
--
ALTER TABLE `proveedores`
  MODIFY `id_proveedor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `rol`
--
ALTER TABLE `rol`
  MODIFY `id_rol` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `detalle`
--
ALTER TABLE `detalle`
  ADD CONSTRAINT `factura_detalle` FOREIGN KEY (`id_factura`) REFERENCES `factura` (`num_factura`),
  ADD CONSTRAINT `producto_detalle` FOREIGN KEY (`id_producto`) REFERENCES `producto` (`id_producto`);

--
-- Filtros para la tabla `factura`
--
ALTER TABLE `factura`
  ADD CONSTRAINT `cliente_factura` FOREIGN KEY (`id_cliente`) REFERENCES `cliente` (`id_cliente`) ON DELETE CASCADE,
  ADD CONSTRAINT `pago_factura` FOREIGN KEY (`num_pago`) REFERENCES `modo_pago` (`num_pago`);

--
-- Filtros para la tabla `producto`
--
ALTER TABLE `producto`
  ADD CONSTRAINT `categoria_producto` FOREIGN KEY (`id_categoria`) REFERENCES `categoria` (`id_categoria`),
  ADD CONSTRAINT `marca_producto` FOREIGN KEY (`id_marca`) REFERENCES `marca` (`id_marca`),
  ADD CONSTRAINT `proveedor_producto` FOREIGN KEY (`id_proveedor`) REFERENCES `proveedores` (`id_proveedor`);

--
-- Filtros para la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `empleado_usuarios` FOREIGN KEY (`id_empleado`) REFERENCES `empleado` (`id_empleado`),
  ADD CONSTRAINT `rol_usuarios` FOREIGN KEY (`id_rol`) REFERENCES `rol` (`id_rol`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
