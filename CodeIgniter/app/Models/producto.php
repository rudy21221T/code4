<?php namespace App\Models;
use CodeIgniter\Model;
use CodeIgniter\Database\ConnectionInterface;


class producto extends Model

{
	public function get_producto(){
		$pa_mostrarCantante ="CALL pa_mostrarCantante()";
		$query = $this->db->query($pa_mostrarCantante);
		if($query){
			return $query->getResult();
		}else{
			return false;
		}
	}

	public function get_sexo(){
		$query = $this->db->table('sexo')->get();
		$query=$query->getResult();
		return $query;
	}

	public function eliminar($id){
		$db  = \Config\Database::connect();
		$builder = $db->table('cantantes');
		$builder->where('id_cantante', $id);
		$builder->delete();
		$message="eliminar";
		echo view('Success',['msj'=>$message]);
	}

	public function set_cantante($datos)
	{
		$db  = \Config\Database::connect();
		$tabla = $db->table('cantantes');
		$data = [
			'nombre' => $datos['nombre'],
			'apellido'  => $datos['apellido'],
			'nombre_artistico' => $datos['nombre_artistico'],
			'id_sexo' => $datos['id_sexo']
		];     
		$tabla->insert($data);   
		$message="add";
		echo  view('Success',['msj'=>$message]);



	}
}